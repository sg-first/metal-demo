#include <metal_stdlib>
#include <simd/simd.h>

using namespace metal;

struct Argument {
    texture2d<half> exampleTexture  [[ id(0)  ]];
    sampler         exampleSampler  [[ id(1)  ]];
    device float   *exampleBuffer   [[ id(2)   ]];
    uint32_t        exampleConstant [[ id(3) ]];
};

kernel void main0(
    device Argument *bufferArray [[buffer(0)]]
) {
    uint32_t aaa = bufferArray[0].exampleConstant;
}